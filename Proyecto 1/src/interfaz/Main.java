package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import parqueadero.Carro;
import parqueadero.Central;
import parqueadero.Queue;
import parqueadero.Stack;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�n, incializa el mundo.
	 * @throws Exception
	 */
	public Main()
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		for (int i = 0; i<40; i++)
		menuInicial();
	}
	
	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() 
	{
		String mensaje = "Men� principal: \n"
			+ "1. Registrar Cliente \n"
			+ "2. Parquear Siguente Carro En Cola \n"
			+ "3. Atender Cliente Que Sale \n"
	        + "4. Ver Estado Parqueaderos \n"
	        + "5. Ver Carros En Cola \n"
			+ "6. Salir \n\n"
			+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			try
			{
				System.out.print(mensaje);
				int op1 = Integer.parseInt(br.readLine());
				while (op1>6 || op1<1)
				{
					System.out.println("\nERROR: Ingrese una opci�n valida\n");
					System.out.print(mensaje);
					op1 = Integer.parseInt(br.readLine());
				}
	
				switch(op1)
				{
					
					case 1: registrarCliente(); break;
					case 2: parquearCarro(); break;
					case 3: salidaCliente(); break;
					case 4: verEstadoParquederos(); break;
					case 5: verCarrosEnCola(); break;
					case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
					
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				continue;
			}
		}

	}

  /**
   * M�todo para registrar a un nuevo cliente
   * @throws Exception
   */
	public void registrarCliente () throws Exception
  {
	  System.out.println("** REGISTRAR CLIENTE **");
	  System.out.println("Nombre del Cliente:");
	  String nombre = br.readLine();
	  System.out.println("Color del carro:");
	  String color = br.readLine();
	  System.out.println("Matrícula del carro:");
	  String matricula = br.readLine();
	  
	  central.registrarCliente(color, matricula, nombre);
	  System.out.println("El vehículo de matrícula " + matricula + " ha sido registrado");
	  
  }
  /**
   * M�todo para parquear un carro que se encuentra en la cola
   * @throws Exception
   */
  public void parquearCarro() throws Exception
  {
	  System.out.println("** PARQUEAR CARRO EN COLA **");
	  System.out.println(central.parquearCarroEnCola());

  }
 /**
  * M�todo para sacar un carro del parqueadero
  * @throws Exception
  */
  public void salidaCliente () throws Exception
  {
	  System.out.println("** REGISTRAR SALIDA CLIENTE **");
	  System.out.println("Matrícula del carro a sacar:");
	  String matricula = br.readLine();
	  System.out.println("Total a pagar:" +central.atenderClienteSale(matricula));
	  
  }
  /**
   * M�todo que permite visualizar graficamente el estado de los parqueaderos
   * @throws Exception
   */
  public void verEstadoParquederos() throws Exception
  {
	  System.out.println("** ESTADO PARQUEADEROS **");
	  System.out.println("Carros Parqueados:");
	  
	  for (int i=0; i<central.getPilaParqueaderos().length; i++)
      {
		  String linea = "P"+ (i+1) + ":    ";
		  while (!central.getPilaParqueaderos()[i].isEmpty())
		  {
		  		linea += central.getPilaParqueaderos()[i].getItemAtTop().darMatricula() + "\t";
		  		central.getPilaParqueaderosTemporal().push(central.getPilaParqueaderos()[i].pop());	
		  }
		  while(!central.getPilaParqueaderosTemporal().isEmpty())
	  		{
			  central.getPilaParqueaderos()[i].push(central.getPilaParqueaderosTemporal().pop());
	  		}
		  System.out.println(linea);
      }
	  String linea = "PT:    ";
	  Stack<Carro> temp = new Stack<>();
	  while(!central.getPilaParqueaderosTemporal().isEmpty())
		{
		  linea += central.getPilaParqueaderosTemporal().getItemAtTop().darMatricula() + "\t";
		  temp.push(central.getPilaParqueaderosTemporal().pop());
		}
	  while(!temp.isEmpty())
		{
		  central.getPilaParqueaderosTemporal().push(temp.pop());
		}
	  System.out.println(linea);
  }
  
  /**
   * M�todo que permite visualizar graficamente el estado de la cola de carros pendientes por parquear
   * @throws Exception
   */
  public void verCarrosEnCola () throws Exception
  {
	  System.out.println("** ESTADO COLA **");
	  System.out.println("Carros en cola:");
	  Queue<Carro> temp = new Queue<>();
	  if (central.getColaCarros().isEmpty())
	  {
		  System.out.println("No hay carros en cola.");
	  }
	  while (!central.getColaCarros().isEmpty())
	  {
		  System.out.println(central.getColaCarros().getItemAtFirst().darMatricula());
		  temp.enqueue(central.getColaCarros().dequeue());
		  
	  }
	  while (!temp.isEmpty())
	  {
		  central.getColaCarros().enqueue(temp.dequeue());
	  }

  }
  
  /**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
				new Main();
	}

}
