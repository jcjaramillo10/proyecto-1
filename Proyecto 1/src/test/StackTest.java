package test;
import parqueadero.*;
import junit.framework.TestCase;


public class StackTest extends TestCase{
	
	private Stack<Carro> stack;
	
	public StackTest()
	{
		stack = new Stack<Carro>();
	}
	
	
	public void setupScenario1 ()
	{

		stack.push(new Carro ("a", "a", "a"));
		stack.push(new Carro ("b", "b", "b"));
		stack.push(new Carro ("c", "c", "c"));
	}
	
	public void testAgregar ()
	{
		assertEquals ("El tamaño no es el indicado", 0, stack.getSize());
		assertNull (stack.getItemAtTop());
		assertTrue (stack.isEmpty());
		
		stack.push(new Carro ("color", "matricula","nombre"));
		assertEquals ("El nombre no es el esperado", "nombre", stack.getItemAtTop().darNombreConductor());
		assertEquals ("El color no es el esperado", "color", stack.getItemAtTop().darColor());
		assertEquals ("La matricula no es el esperado", "matricula", stack.getItemAtTop().darMatricula());
		assertEquals ("El tamaño no es el indicado", 1, stack.getSize());
		assertNotNull (stack.getItemAtTop());
		assertFalse (stack.isEmpty());
		
	}
	
	public void testEliminar ()
	{
		setupScenario1();
		assertEquals ("El tamaño no es el indicado", 3, stack.getSize());
		assertNotNull (stack.getItemAtTop());
		assertFalse (stack.isEmpty());
		
		Carro eliminado = stack.pop();
		assertEquals ("El nombre del carro eliminado no es el esperado", "c", eliminado.darNombreConductor());
		assertEquals ("El color del color eliminado no es el esperado", "c", eliminado.darColor());
		assertEquals ("La matricula del carro eliminado no es el esperado", "c", eliminado.darMatricula());
		assertEquals ("El nombre del primer elemento no es el esperado", "b", stack.getItemAtTop().darNombreConductor());
		assertEquals ("El color del primer elemento no es el esperado", "b", stack.getItemAtTop().darColor());
		assertEquals ("La matricula del primer elemento no es el esperado", "b", stack.getItemAtTop().darMatricula());
		assertEquals ("El tamaño no es el indicado", 2, stack.getSize());
		assertNotNull (stack.getItemAtTop());
		assertFalse (stack.isEmpty());
		
	}


}
