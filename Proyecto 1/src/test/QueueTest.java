package test;
import parqueadero.*;
import junit.framework.TestCase;

public class QueueTest extends TestCase {

	private Queue<Carro> queue;
	
	public QueueTest()
	{
		queue = new Queue<Carro>();
	}
	
	
	public void setupScenario1 ()
	{
		queue.enqueue(new Carro ("a", "a", "a"));
		queue.enqueue(new Carro ("b", "b", "b"));
		queue.enqueue(new Carro ("c", "c", "c"));
	}
	
	public void testAgregar ()
	{
		assertEquals ("El tamaño no es el indicado", 0, queue.getSize());
		assertNull (queue.getItemAtFirst());
		assertTrue (queue.isEmpty());
		
		queue.enqueue(new Carro ("color", "matricula","nombre"));
		assertEquals ("El nombre no es el esperado", "nombre", queue.getItemAtFirst().darNombreConductor());
		assertEquals ("El color no es el esperado", "color", queue.getItemAtFirst().darColor());
		assertEquals ("La matricula no es el esperado", "matricula", queue.getItemAtFirst().darMatricula());
		assertEquals ("El tamaño no es el indicado", 1, queue.getSize());
		assertNotNull (queue.getItemAtFirst());
		assertFalse (queue.isEmpty());
		
	}
	
	public void testEliminar ()
	{
		setupScenario1();
		assertEquals ("El tamaño no es el indicado", 3, queue.getSize());
		assertNotNull (queue.getItemAtFirst());
		assertFalse (queue.isEmpty());
		
		Carro eliminado = queue.dequeue();
		assertEquals ("El nombre del carro eliminado no es el esperado", "a", eliminado.darNombreConductor());
		assertEquals ("El color del color eliminado no es el esperado", "a", eliminado.darColor());
		assertEquals ("La matricula del carro eliminado no es el esperado", "a", eliminado.darMatricula());
		assertEquals ("El nombre del primer elemento no es el esperado", "b", queue.getItemAtFirst().darNombreConductor());
		assertEquals ("El color del primer elemento no es el esperado", "b", queue.getItemAtFirst().darColor());
		assertEquals ("La matricula del primer elemento no es el esperado", "b", queue.getItemAtFirst().darMatricula());
		assertEquals ("El tamaño no es el indicado", 2, queue.getSize());
		assertNotNull (queue.getItemAtFirst());
		assertFalse (queue.isEmpty());
		
	}

}
