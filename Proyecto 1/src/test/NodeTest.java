package test;

import junit.framework.TestCase;
import parqueadero.Carro;
import parqueadero.Node;


public class NodeTest extends TestCase{
	
	private Node<Carro> node;
	
	public NodeTest()
	{
		node = new Node<Carro>();
	}
	
	
	public void setupScenario1 ()
	{
		node.setItem(new Carro ("a", "a", "a"));
		node.setNext(new Node<Carro>(new Carro ("b", "b", "b"), null));
		node.setNext(new Node<Carro>(new Carro ("c", "c", "c"), null));

	}
	
	public void testAgregar ()
	{
		assertNull (node.getItem());
		assertNull (node.getNext());

		
		node.setItem(new Carro ("color", "matricula","nombre"));
		assertEquals ("El nombre no es el esperado", "nombre", node.getItem().darNombreConductor());
		assertEquals ("El color no es el esperado", "color", node.getItem().darColor());
		assertEquals ("La matricula no es el esperado", "matricula", node.getItem().darMatricula());
		assertNotNull (node.getItem());
		assertNull (node.getNext());
		
	}
	
	public void testEliminar ()
	{
		setupScenario1();
		assertNotNull (node.getItem());
		assertNotNull (node.getNext());
		
		node.setNext(null);
		assertNull(node.getNext());
	}



}
