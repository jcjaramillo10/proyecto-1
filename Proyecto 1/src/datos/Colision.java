package datos;

public class Colision {
	
	private String nombre;
	
	private String autoridad;
	
	private int fatal;
	
	private int serious;
	
	private int slight;
	
	private int total;
	
	private int año;

	public Colision(String nombre, String autoridad) {
		super();
		this.nombre = nombre;
		this.autoridad = autoridad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAutoridad() {
		return autoridad;
	}

	public void setAutoridad(String autoridad) {
		this.autoridad = autoridad;
	}

	public int getFatal() {
		return fatal;
	}

	public void setFatal(int fatal) {
		this.fatal = fatal;
	}

	public int getSerious() {
		return serious;
	}

	public void setSerious(int serious) {
		this.serious = serious;
	}

	public int getSlight() {
		return slight;
	}

	public void setSlight(int slight) {
		this.slight = slight;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getAño() {
		return año;
	}

	public void setAño(int año) {
		this.año = año;
	}
	
	

}
