package parqueadero;

/**
 * Clase que describe la pila con el protocolo LIFO
 * Este código es proporcionado por el profesor y el libro
 * package: estructuras-lineales.co.edu.uniandes.collections.linear
 */
public class Stack<T> {
	
	private Node<T> top;
	private int size;
	
	public Stack(){
		this.top = null;
		this.size = 0;
	}
	
	
	public T getItemAtTop() {
		if(isEmpty()){
			return null;
		}
		return top.getItem();
	}

	
	public boolean isEmpty(){
		return top == null;
	}
	
	
	public int getSize() {
		return size;
	}
	

	public void push(T item){
		top = new Node<T>(item, top);
		size++;
	}

	public T pop(){
		T item = null;
		if(top != null){
		    item = top.getItem();
			top = top.getNext();
			size--;
		}
		return item;
	}
	


	
	
	public static void main(String[ ] args){
		Stack<String> stack = new Stack<>();
		
		stack.push("Hola");
		stack.push("amigo");
		stack.push("chepe");
		
		String item = null;
		while( (item = stack.pop()) != null){
		
			System.out.println(item);
			System.out.println(stack.getSize());
		}
	}
}
	


