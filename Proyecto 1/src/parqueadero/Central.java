package parqueadero;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	private Queue<Carro> colaCarros;
	
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Stack<Carro>[] pilaParqueaderos; 

    
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	private Stack<Carro> pilaParqueaderosTemporal; 
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */

    
    public Central() {
		super();
		colaCarros = new Queue<Carro>();
		pilaParqueaderos = new Stack[5];
		for (int i = 0; i<5; i++)
		{
			pilaParqueaderos[i] = new Stack<Carro>();
		}
		pilaParqueaderosTemporal = new Stack<>();
	}


	public Queue<Carro> getColaCarros() {
		return colaCarros;
	}


	public Stack<Carro>[] getPilaParqueaderos() {
		return pilaParqueaderos;
	}


	public Stack<Carro> getPilaParqueaderosTemporal() {
		return pilaParqueaderosTemporal;
	}


	/**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	colaCarros.enqueue(new Carro(pColor, pMatricula, pNombreConductor));
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	if (colaCarros.getItemAtFirst() != null)
    	{
	    	try
	    	{
	    		String resp = parquearCarro(colaCarros.getItemAtFirst());
	    		colaCarros.dequeue();
	    		return resp;
	    	}	
	    	catch (Exception e)
	    	{
	    		throw new Exception(e.getMessage());
	    	}
    	}
    	else
    		throw new Exception("Error:" + "\n" + "No hay ningún parqueadero en cola");
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	Carro sacar = sacarCarro(matricula);
    	if (sacar != null)
    		return cobrarTarifa(sacar);
    	else
    		throw new Exception ("Error:" + "\n" +"No se encontro el vehículo con matrícula " + matricula); 
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	for (int i=0; i<pilaParqueaderos.length; i++)
        {
  		  Stack<Carro> actual = pilaParqueaderos[i];
  		  if( actual.getSize() < 4)
  		  {
  			  actual.push(aParquear);
  			  return "el vehiculo " + aParquear.darMatricula() + " ha sido registrado en el parqueadero "+ (i+1);
  			  
  		  }
        }
        throw new Exception ("Error:" + "\n" +"No se pudo parquear el carro, todos los parqueaderos estan llenos");	    
    }
    
    /**
     * Itera sobre los ocho parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	for (int i=0; i<pilaParqueaderos.length; i++)
        {

    		while (!pilaParqueaderos[i].isEmpty())
    		{
	  		  if (pilaParqueaderos[i].getItemAtTop().darMatricula().equals(matricula))
	  		  {
	  			Carro sacar = pilaParqueaderos[i].pop();
	  			while(!pilaParqueaderosTemporal.isEmpty())
	    		{
	    			pilaParqueaderos[i].push(pilaParqueaderosTemporal.pop());
	    		}
	  			  return sacar;
	  		  }
	  		  else
	  		  {
	  			  pilaParqueaderosTemporal.push(pilaParqueaderos[i].pop());
	  		  }
    		} 
    		while(!pilaParqueaderosTemporal.isEmpty())
    		{
    			pilaParqueaderos[i].push(pilaParqueaderosTemporal.pop());
    		}
        }
    	return null;
    }
    	
  
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	return ((System.currentTimeMillis() - car.darLlegada())/60000) * 25;
    }
}
